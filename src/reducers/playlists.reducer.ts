import { Reducer, Action, ActionCreator } from "redux";
import { Playlist } from "../model/playlist";

export type PlaylistsState = {
  playlists: Playlist[];
  selectedId?: Playlist["id"];
};

const initialState: PlaylistsState = {
  playlists: [
    {
      id: 123,
      name: "React Hits",
      favourite: false,
      color: "#ff0000"
    },
    {
      id: 234,
      name: "React TOP 20",
      favourite: true,
      color: "#00ff00"
    },
    {
      id: 345,
      name: "Best of React",
      favourite: false,
      color: "#0000ff"
    }
  ]
};

export const playlists: Reducer<PlaylistsState, ActionTypes> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "PLAYLISTS_ADD":
      return {
        ...state,
        playlists: [...state.playlists, action.payload]
      };
    case "PLAYLISTS_SELECT":
      return {
        ...state,
        selectedId: action.payload
      };
    case "PLAYLISTS_UPDATE":
      return {
        ...state,
        playlists: state.playlists.map(
          playlist =>
            playlist.id === action.payload.id ? action.payload : playlist
        )
      };
    case "PLAYLISTS_DELETE":
      return {
        ...state,
        playlists: state.playlists.filter(
          playlist => playlist.id !== action.payload
        )
      };
    default:
      return state;
  }
};

type ActionTypes =
  | PLAYLISTS_ADD
  | PLAYLISTS_UPDATE
  | PLAYLISTS_DELETE
  | PLAYLISTS_SELECT;

interface PLAYLISTS_ADD extends Action<"PLAYLISTS_ADD"> {
  payload: Playlist;
}

interface PLAYLISTS_UPDATE extends Action<"PLAYLISTS_UPDATE"> {
  payload: Playlist;
}

interface PLAYLISTS_SELECT extends Action<"PLAYLISTS_SELECT"> {
  payload: Playlist["id"];
}

interface PLAYLISTS_DELETE extends Action<"PLAYLISTS_DELETE"> {
  payload: Playlist["id"];
}

// export const addPlaylist = (payload: Playlist):PLAYLISTS_ADD => ({
//   type: 'PLAYLISTS_ADD',
//   payload
// });

export const addPlaylist: ActionCreator<PLAYLISTS_ADD> = (
  payload: Playlist
) => ({
  type: "PLAYLISTS_ADD",
  payload
});

export const selectPlaylist: ActionCreator<PLAYLISTS_SELECT> = (
  payload: Playlist["id"]
) => ({
  type: "PLAYLISTS_SELECT",
  payload
});

export const updatePlaylist: ActionCreator<PLAYLISTS_UPDATE> = (
  payload: Playlist
) => ({
  type: "PLAYLISTS_UPDATE",
  payload
});

export const deletePlaylist: ActionCreator<PLAYLISTS_DELETE> = (
  payload: Playlist["id"]
) => ({
  type: "PLAYLISTS_DELETE",
  payload
});

export const playlistsActions = {
  add: addPlaylist,
  select: selectPlaylist,
  update: updatePlaylist,
  delete: deletePlaylist
};

window["playlistsActions"] = playlistsActions;

// export const actions: ActionCreatorsMapObject<ActionTypes> = {
//   addPlaylist(payload: Playlist) {
//     return {
//       type: "PLAYLISTS_ADD",
//       payload
//     };
//   }
// };
