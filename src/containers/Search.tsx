import { connect } from "react-redux";
import { SearchField } from "../components/SearchField";
import { SearchResults } from "../components/SearchResults";
import { Album } from "src/model/Album";
import { Dispatch } from "redux";
import { AppState } from "../store";
import { searchAlbums } from "../services";

type Props = {
  results: Album[];

  loading?: boolean;
  error?: string;
};
type DispatchProps = {
  onSearch(query: string): void;
};

const withSearch = connect<Props, DispatchProps, {}, AppState>(
  (state: AppState) => ({
    results: state.search.results,
    loading: state.search.loading,
    error: state.search.error,
  }),
  (dispatch: Dispatch) => ({
    // onSearch(query: string) {
    //   searchAlbums(dispatch)(query);
    // }
    onSearch: searchAlbums(dispatch)
  })
);

export const AlbumSearchField = withSearch(SearchField);
export const AlbumSearchResults = withSearch(SearchResults);
