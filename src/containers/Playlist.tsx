import { connect } from "react-redux";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistForm } from "../components/PlaylistForm";
import { Dispatch } from "redux";
import { AppState } from "src/store";
import { Playlist } from "src/model/playlist";
import { updatePlaylist, deletePlaylist } from "../reducers/playlists.reducer";

type Props = {
  playlist?: Playlist;
};

type DispatchProps = {
  onSave(playlist: Playlist): void;
  onRemove(playlist: Playlist): void;
};
type OwnProps = {
  onSave?(playlist: Playlist): void;
  onRemove?(playlist: Playlist): void;
};

const withPlaylist = connect<Props, DispatchProps, OwnProps, AppState>(
  state => ({
    playlist: state.playlists.playlists.find(
      playlist => playlist.id === state.playlists.selectedId
    )
  }),
  (dispatch: Dispatch, ownProps: OwnProps) => ({
    onSave(playlist: Playlist) {
      dispatch(updatePlaylist(playlist));
      if (ownProps.onSave) {
        ownProps.onSave(playlist);
      }
    },
    onRemove(playlist: Playlist) {
      dispatch(deletePlaylist(playlist.id));
    }
  })
);

export const SelectedPlaylistDetails = withPlaylist(PlaylistDetails);
export const SelectedPlaylistForm = withPlaylist(PlaylistForm);
