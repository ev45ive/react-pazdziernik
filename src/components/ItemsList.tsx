import * as React from "react";

export type Item = {
  id: number;
  name: string;
};

export type Props = {
  items: Item[];
  selected?: Item;
  onSelect(item: Item): void;
  onRemove?(item: Item): void;
};

export class ItemsList extends React.Component<Props> {
  render() {
    // console.log('Container updated!')
    return (
      <div>
        <div className="list-group">
          {/* tslint:disable jsx-no-lambda */}
          {this.props.items.map((item, index) => (
            <div
              className={`list-group-item ${
                this.props.selected && item.id == this.props.selected.id
                  ? "active"
                  : ""
              }`}
              key={item.id}
              onClick={() => this.props.onSelect(item)}
            >
              {index + 1}. {item.name}
              <span
                className="close"
                onClick={() => this.props.onRemove && this.props.onRemove(item)}
              >
                &times;
              </span>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
