import * as React from "react";
import { Playlist } from "../model/playlist";
import { Card } from "./Card";

type Props = {
  playlist?: Playlist;
  onEdit(): void;
};

export class PlaylistDetails extends React.Component<Props> {
  componentDidMount() {
    // console.log("componentDidMount");
  }

  //@deprecated componentWillUpdate
  getSnapshotBeforeUpdate() {
    // console.log("getSnapshotBeforeUpdate");
    return { placki: 123 };
  }

  // After DOM updated
  componentDidUpdate(props: Props, state: {}, snapshot: {}) {
    // console.log("componentDidUpdate",snapshot);
  }

  componentWillUnmount() {
    // console.log("componentWillUnmount");
  }

  render() {
    return this.props.playlist ? (
      <Card title="Playlist Details">
        <div>
          <dl>
            <dt>Name:</dt>
            <dd>{this.props.playlist.name}</dd>

            <dt>Favourite:</dt>
            <dd>{this.props.playlist.favourite ? "Yes" : "No"} </dd>

            <dt>Color:</dt>
            <dd
              style={{
                color: this.props.playlist.color
              }}
            >
              {this.props.playlist.color}
            </dd>
          </dl>

          <input
            type="button"
            value="Edit"
            className="btn btn-info"
            onClick={this.props.onEdit}
          />
        </div>
      </Card>
    ) : (
      <p>Please select playlist</p>
    );
  }
}
