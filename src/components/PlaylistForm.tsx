import * as React from "react";
import { Playlist } from "../model/playlist";
import { Card } from "./Card";

type Props = {
  playlist?: Playlist;
  // onChange?(playlist: Playlist): void;
  onSave(playlist: Playlist): void;
  onCancel(): void;
};

type State = {
  draft: Playlist;
};

export class PlaylistForm extends React.Component<Props, State> {
  state: State = {
    draft: this.props.playlist
      ? { ...this.props.playlist }
      : {
          id: 0,
          name: "",
          favourite: false,
          color: "#00000"
        }
  };

  constructor(props: Props) {
    super(props);
  }

  static getDerivedStateFromProps(nextProps: Props, state: State) {
    return nextProps.playlist && state.draft.id != nextProps.playlist.id
      ? {
          draft: { ...nextProps.playlist }
        }
      : {};
  }
  // componentWillReceiveProps(nextProps: Props) {
  //   const draft = { ...nextProps.playlist };

  //   this.setState({
  //     draft
  //   });
  // }

  handleFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.target;
    const fieldName = target.name;
    const type = target.type;
    const value = type == "checkbox" ? target.checked : target.value;

    // this.props.playlist[fieldName] = value;
    // this.props.onChange(this.props.playlist)
    // VS:
    this.setState(nextState => ({
      draft: {
        ...nextState.draft,
        [fieldName]: value
      }
    }));
  };

  save = () => {
    this.props.onSave(this.state.draft);
  };

  componentDidMount() {
    if (this.inputRef.current) {
      this.inputRef.current.focus();
    }
  }

  inputRef = React.createRef<HTMLInputElement>();

  render() {
    return (
      <Card title="Playlist Form">
        <div>
          {/* .form-group*3>label+input.form-control */}

          <div className="form-group">
            <label>Name:</label>
            <input
              ref={this.inputRef}
              type="text"
              name="name"
              className="form-control"
              onChange={this.handleFieldChange}
              value={this.state.draft.name}
            />
          </div>

          <div className="form-group">
            <label>Favourite:</label>
            <input
              type="checkbox"
              name="favourite"
              onChange={this.handleFieldChange}
              checked={this.state.draft.favourite}
            />
          </div>

          <div className="form-group">
            <label>Color:</label>
            <input
              type="color"
              name="color"
              onChange={this.handleFieldChange}
              value={this.state.draft.color}
            />
          </div>

          <input
            type="button"
            value="Cancel"
            className="btn"
            onClick={this.props.onCancel}
          />
          <input
            type="button"
            value="Save"
            className="btn"
            onClick={this.save}
          />
        </div>
      </Card>
    );
  }
}
