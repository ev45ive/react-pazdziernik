import * as React from "react";
import "bootstrap/dist/css/bootstrap.css";
import { PlaylistsView } from "./views/PlaylistsView";
import { MusicSearch } from "./views/MusicSearch";
import { Route, Redirect, Switch, Link, NavLink } from "react-router-dom";

class App extends React.Component {
  public render() {
    return (
      <>
        <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
          <div className="container">
            <Link className="navbar-brand" to="/">
              Music App
            </Link>

            <div className="collapse navbar-collapse">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    activeClassName="active placki"
                    to="/playlists"
                  >
                    Playlists
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/search">
                    Search
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        <div className="container">
          <div className="row">
            <div className="col">
              <Switch>
                <Route
                  path="/playlists/:id"
                  component={PlaylistsView}
                />
                <Route path="/search" component={MusicSearch} />
                <Redirect from="/" exact={true} to="playlists" />
                <Route
                  from="**"
                  exact={true}
                  render={() => (
                    <h2 className="mt-4 text-center">
                      404 - Nothing interesting here
                    </h2>
                  )}
                />
              </Switch>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default App;
