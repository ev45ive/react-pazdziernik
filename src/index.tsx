import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";
import { security } from "./services";
import { store, history } from "./store";
import { Provider } from "react-redux";

// import { HashRouter as Router } from "react-router-dom";
// import { BrowserRouter as Router } from "react-router-dom";

import { ConnectedRouter as Router } from "connected-react-router";

security.getToken();
store;

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <App />
    </Router>
  </Provider>,
  document.getElementById("root") as HTMLElement
);
registerServiceWorker();

// interface IWindow {
//   [k: string]: any;
// }

// (window as IWindow)["React"] = React;
// (window as IWindow)["ReactDOM"] = ReactDOM;
