interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  artists?: Artist[];
  images: AlbumImage[];
}

export interface Artist extends Entity {
  followers: number;
}

export interface AlbumImage {
  url: string;
  width?: number;
  height?: number;
}

export interface PagingObject<T> {
  items: T[];
  limit: number;
  total: number;
  offset: number;
}

export type AlbumsResponse = {
  albums: PagingObject<Album>;
};
