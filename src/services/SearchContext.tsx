import * as React from "react";
import { Album } from "../model/Album";
import { search } from "src/services";

export const SearchContext = React.createContext({
  search(query: string): void {
    throw "No SearchProvider Found";
  },
  results: [] as any[]
});

type State = {
  results: Album[];
  query: string;
  error?: string;
  loading?: boolean;
};

export class SearchProvider extends React.Component<{}, State> {
  state: State = {
    results: [],
    query: ""
  };

  search = (query: string) => {
    if (!query) {
      return;
    }
    this.setState({
      loading: true,
      error: ""
    });

    search.search(query).then(
      results => {
        this.setState({
          results,
          loading: false
        });
      },
      error => {
        this.setState({
          error,
          loading: false
        });
      }
    );
  };

  render() {
    return (
      <SearchContext.Provider
        value={{
          search: this.search,
          results: this.state.results
        }}
      >
        {this.props.children}
      </SearchContext.Provider>
    );
  }
}

export const withSearch = (
  Component: React.ComponentType<{
    onSearch?(query: string): void;
    results?: any[];
  }>
) => () => (
  <>
    <SearchContext.Consumer>
      {props => <Component onSearch={props.search} results={props.results} />}
    </SearchContext.Consumer>
  </>
);
